#include <QCoreApplication>
#include<cipher.h>
#include<sql.h>
#include<crc32.h>
#include "definitions.h"
#include "userinfo.h"

int main(int argc, char *argv[])
{  QCoreApplication a(argc, argv);
    printf("Start Generation ...... ");

    QTime myTimer;
    myTimer.start();
    Sql SqlObj;
    QSqlDatabase db=SqlObj.SqlConnection();
    cipher cipherObj;
    crc32 crcObj;
    UserInfo UserObj;
    UserObj.GetUser();
for(int i =0 ; i <=1 ;i++){

    cipherObj.GeneratePrimaryKeys();
    cipherObj.GenerateSecondaryKeys();
    PUBPPK_CRC=crcObj.CalculateCRC32(PUBPPK_Hash);

    SqlObj.SqlInsert();
}
SqlObj.CloseConnection(db);
 quint64 _Timer =  myTimer.elapsed();
 QString str = QString::number(_Timer);
  qDebug() << "This operation took" << myTimer.elapsed() << "milliseconds";


    return a.exec();
}

