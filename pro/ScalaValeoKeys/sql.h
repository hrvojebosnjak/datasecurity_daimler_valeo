#ifndef SQL_H
#define SQL_H

#include <QtCore/QCoreApplication>
#include <QtSql>

const QString ServerName ="DESKTOP-D5TR94N";
const QString dbName ="QTDemoDB";
const QString dbDriver=  "QODBC";
class Sql
{
public:
    Sql();
    QSqlDatabase SqlConnection();
    void SqlInsert();
    void CloseConnection(QSqlDatabase db);
};

#endif // SQL_H
