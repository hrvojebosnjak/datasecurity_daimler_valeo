#ifndef CIPHER_H
#define CIPHER_H
#include <QObject>
#include <QDebug>
#include <QFile>
#include <openssl/rsa.h>
#include <openssl/engine.h>
#include <openssl/pem.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <QCryptographicHash>
#define KEY_LENGTH  4096
#define PUB_EXP     3
#define PRINT_KEYS
#define WRITE_TO_FILE

class cipher
{
public:
    cipher();
     void GeneratePrimaryKeys();
      void GenerateSecondaryKeys();
private:
    void initalize();
    void finalize();

};

#endif // CIPHER_H
