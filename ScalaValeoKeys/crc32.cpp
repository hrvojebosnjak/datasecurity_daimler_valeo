#include "crc32.h"

crc32::crc32()
{

    quint32 crc;

    // initialize CRC table
    for (int i = 0; i < 256; i++)
    {
        crc = i;
        for (int j = 0; j < 8; j++)
            crc = crc & 1 ? (crc >> 1) ^ 0xEDB88320UL : crc >> 1;

        crc_table[i] = crc;
    }
}
/*! \CalculateCRC32.
 */
QString crc32::CalculateCRC32(QString InputValue)
{

    quint32 crc;


    char  buffer[16000];
    int len, i;

    crc = 0xFFFFFFFFUL;


     QByteArray ba = InputValue.toLatin1();

     strcpy(buffer, ba);
     len= InputValue.length();

     for (i = 0; i < len; i++)
         crc = crc_table[(crc ^ buffer[i]) & 0xFF] ^ (crc >> 8);
     quint32 m = crc ^ 0xFFFFFFFFUL ;
     QString result = QString::number( m, 16 );

     return result.toUpper();
}
