#include "sql.h"
#include "definitions.h"
Sql::Sql()
{

}
/*! \SqlConnection.
 *         Set database connections parameter.
 * open database connection
 * call insert function
 * close database connection
 */
QSqlDatabase Sql::SqlConnection()
{
    QSqlDatabase db = QSqlDatabase::addDatabase(dbDriver);

    db.setConnectOptions();
        //  qDebug () << "ODBC driver valid?" << db.isValid ();
      QString dsn = QString("DRIVER={SQL Server};SERVER=%1;DATABASE=%2;Trusted_Connection=Yes;").arg(ServerName).arg((dbName));
      db.setDatabaseName(dsn);
       db.open();
       return db;
}
void Sql::CloseConnection(QSqlDatabase db){
 db.close();
}
/*! \SqlInsert.
 *        Insert all global variable into SQL server.
 */
void Sql::SqlInsert()
{

        QSqlQuery qry;


            qry.prepare("INSERT INTO KeyGen_Table(PPK ,PSK, SPK , SSK, PPK_Hash,PPK_CRC, User_Name,IP_address, Machine_Name)"
                           "VALUES(:PPK,:PSK,:SPK,:SSK,:PPK_Hash,:PPK_CRC,:User_Name,:IP_address , :Machine_Name)");

            qry.bindValue(":PPK",PUBPPK);
            qry.bindValue(":PSK",PUBPSK);
            qry.bindValue(":SPK",PUBSPK);
            qry.bindValue(":SSK",PUBSSK);
            qry.bindValue(":PPK_Hash",PUBPPK_Hash);
            qry.bindValue(":PPK_CRC",PUBPPK_CRC);
            qry.bindValue(":User_Name",PUBUser_Name);
            qry.bindValue(":IP_address",PUBIP_address);
            qry.bindValue(":Machine_Name",PUBMachine_Name);
            if( !qry.exec() )
                qDebug() << qry.lastError().text();
           // else
               // qDebug( "Inserted!" );


}

