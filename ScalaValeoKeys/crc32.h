#ifndef CRC32_H
#define CRC32_H

#include <QtCore>
#include <QString>
class crc32
{
private:
    quint32 crc_table[256];

public:
    crc32();

    QString CalculateCRC32(QString InputValue);

};

#endif // CRC32_H
